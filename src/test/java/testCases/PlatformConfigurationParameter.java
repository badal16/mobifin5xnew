package testCases;

import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.SetupInit;
import base.TestDataImport;
import pages.CommonPage;
import pages.HomePage;
import pages.NavigationPage;
import pages.PlatformConfigurationParameterPage;
import utils.Utility;

public class PlatformConfigurationParameter extends SetupInit {
	HomePage homePage;
	NavigationPage navigationPage;
	PlatformConfigurationParameterPage platformConfigurationParameterPage;
	CommonPage common;
	private int sortCounter;

	@BeforeMethod
	public void beforeMethod() {
		homePage = new HomePage(getDriver());
		navigationPage = new NavigationPage(getDriver());
		platformConfigurationParameterPage = new PlatformConfigurationParameterPage(getDriver());
	}

	@Test(dataProvider = "Parameter_Add", dataProviderClass = TestDataImport.class, description = "Id: AddParameter, Author: shivani.patel")
	public void addParameter(Map<Object, Object> map) {
		try {
			setTestParameters(map, "addParameter");
			homePage.goToHome();
			navigationPage.clickOnPlateformConfigurationParameter();
			platformConfigurationParameterPage.addParameter(map, Utility.getMapKeys(map));
			platformConfigurationParameterPage.verifyAddedParameter(map, Utility.getMapKeys(map));
			setSuccessParameters(map);
		} catch (Exception e) {
			setFailureParameters(map);
			logException(e, map);
		} finally {
			logData(map);
		}
		System.out.println("Add Parameter");
	}

	@Test(dataProvider = "Parameter_Edit", dataProviderClass = TestDataImport.class, description = "Id: EditParameter, Author: shivani.patel")
	public void editParameter(Map<Object, Object> map) {
		try {
			setTestParameters(map, "editParameter");
			homePage.goToHome();
			navigationPage.clickOnPlateformConfigurationParameter();
			if (platformConfigurationParameterPage.editParameter(map, Utility.getMapKeys(map)))
				platformConfigurationParameterPage.verifyEditedParameter(map, Utility.getMapKeys(map));
			else
				throw new Exception("Method has return false : Record not found");
			setSuccessParameters(map);
		} catch (Exception e) {
			setFailureParameters(map);
			logException(e, map);
		} finally {
			logData(map);
		}
	}

	@Test(dataProvider = "Parameter_Delete", dataProviderClass = TestDataImport.class, description = "Id: DeleteParameter, Author: shivani.patel")
	public void deleteParameter(Map<Object, Object> map) {
		try {
			setTestParameters(map, "deleteParameter");
			homePage.goToHome();
			navigationPage.clickOnPlateformConfigurationParameter();
			if (platformConfigurationParameterPage.deleteParameter(map, Utility.getMapKeys(map)))
				if (platformConfigurationParameterPage.verifyDeletedParameter(map, Utility.getMapKeys(map)))
					throw new Exception("Method has return false : Record not deleted");
			setSuccessParameters(map);
		} catch (Exception e) {
			setFailureParameters(map);
			logException(e, map);
		} finally {
			logData(map);
		}
	}

	@Test(dataProvider = "Parameter_Sort", dataProviderClass = TestDataImport.class, description = "Id: SortParameter, Author: shivani.patel")
	public void sortParameter(Map<Object, Object> map) {
		try {
			setTestParameters(map, "sortParameter");
			homePage.goToHome();
			navigationPage.clickOnPlateformConfigurationParameter();
			if (sortCounter == 0) {
				sortCounter++;
				List<String> list = common.addColumnInGrid();
				if (common.verifyColumnInGrid(list))
					booleanValue = true;
			}
			if (booleanValue) {
				if (!platformConfigurationParameterPage.sortParameter(map, Utility.getMapKeys(map)))
					throw new Exception("Method has return false : sorting failed");
			} else
				throw new Exception("Method has return false : columns mis-matched");
			setSuccessParameters(map);
		} catch (Exception e) {
			setFailureParameters(map);
			logException(e, map);
		} finally {
			logData(map);
		}
	}
}
